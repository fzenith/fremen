# FREMEN #
## Fluid Recovery Enabling Methanol Energy in eNvironment ##

This is a collection of dynamic models in Modelica that have been used in several scientific publications on DMFC systems.
The code was originally written by Federico Zenith at the Max Planck Institute for Dynamics of Complex Technical Systems in Magdeburg, 

The code was developed in Dymola, but should run in any Modelica environment such as OpenModelica. Compatibility patches are most welcome!

# Licence #
The code is licensed according to the GNU General Public Licence, version 3 or later at your choice.

# Contact #
Federico Zenith is the owner of the repository and your contact for any questions or suggestions.